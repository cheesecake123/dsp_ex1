/********************************************************************************
** Form generated from reading UI file 'dspmainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPMAINWINDOW_H
#define UI_DSPMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_DSPMainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QCustomPlot *plot1;
    QCustomPlot *plot2;
    QHBoxLayout *horizontalLayout;
    QLineEdit *freqInput2;
    QLineEdit *freqInput1;
    QLineEdit *freqInput3;
    QLineEdit *freqInput4;
    QLineEdit *freqInput5;
    QPushButton *plotButton;
    QLabel *label_2;
    QLabel *label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DSPMainWindow)
    {
        if (DSPMainWindow->objectName().isEmpty())
            DSPMainWindow->setObjectName(QStringLiteral("DSPMainWindow"));
        DSPMainWindow->resize(1200, 700);
        centralWidget = new QWidget(DSPMainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(5);
        gridLayout->setContentsMargins(15, 15, 15, 15);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        plot1 = new QCustomPlot(centralWidget);
        plot1->setObjectName(QStringLiteral("plot1"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(plot1->sizePolicy().hasHeightForWidth());
        plot1->setSizePolicy(sizePolicy);

        gridLayout->addWidget(plot1, 1, 0, 1, 1);

        plot2 = new QCustomPlot(centralWidget);
        plot2->setObjectName(QStringLiteral("plot2"));

        gridLayout->addWidget(plot2, 1, 1, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(5);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        freqInput2 = new QLineEdit(centralWidget);
        freqInput2->setObjectName(QStringLiteral("freqInput2"));

        horizontalLayout->addWidget(freqInput2);

        freqInput1 = new QLineEdit(centralWidget);
        freqInput1->setObjectName(QStringLiteral("freqInput1"));

        horizontalLayout->addWidget(freqInput1);

        freqInput3 = new QLineEdit(centralWidget);
        freqInput3->setObjectName(QStringLiteral("freqInput3"));

        horizontalLayout->addWidget(freqInput3);

        freqInput4 = new QLineEdit(centralWidget);
        freqInput4->setObjectName(QStringLiteral("freqInput4"));

        horizontalLayout->addWidget(freqInput4);

        freqInput5 = new QLineEdit(centralWidget);
        freqInput5->setObjectName(QStringLiteral("freqInput5"));

        horizontalLayout->addWidget(freqInput5);

        plotButton = new QPushButton(centralWidget);
        plotButton->setObjectName(QStringLiteral("plotButton"));

        horizontalLayout->addWidget(plotButton);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 2);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 0, 1, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        DSPMainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DSPMainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1200, 21));
        DSPMainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(DSPMainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        DSPMainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DSPMainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DSPMainWindow->setStatusBar(statusBar);

        retranslateUi(DSPMainWindow);

        QMetaObject::connectSlotsByName(DSPMainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *DSPMainWindow)
    {
        DSPMainWindow->setWindowTitle(QApplication::translate("DSPMainWindow", "DSPMainWindow", Q_NULLPTR));
        plotButton->setText(QApplication::translate("DSPMainWindow", "PushButton", Q_NULLPTR));
        label_2->setText(QApplication::translate("DSPMainWindow", "Frequency domain:", Q_NULLPTR));
        label->setText(QApplication::translate("DSPMainWindow", "Signal plot:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DSPMainWindow: public Ui_DSPMainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPMAINWINDOW_H
