#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DSPMainWindow)
{
    ui->setupUi(this);
	connect(ui->plotButton, SIGNAL(clicked()), this, SLOT(plotButton()));
}

DSPMainWindow::~DSPMainWindow()
{
    delete ui;
}

double sine(double x, double A, double f)
{
	return A*sin(2.0 * M_PI * f * x + 0.0);
}

void DSPMainWindow::plotButton()
{
	int freq1 = ui->freqInput1->text().toInt();
	int freq2 = ui->freqInput2->text().toInt();
	int freq3 = ui->freqInput3->text().toInt();
	int freq4 = ui->freqInput4->text().toInt();
	int freq5 = ui->freqInput5->text().toInt();

	QVector<double> xAxis, yAxis;

	for (double i = 0; i <= 44100; i++)
	{
		double x = i / 44100;
		xAxis.push_back(x);
		double y = sine(x, 0.2, freq1)
				+ sine(x, 0.2, freq2)
				+ sine(x, 0.2, freq3)
				+ sine(x, 0.2, freq4)
				+ sine(x, 0.2, freq5);
		yAxis.push_back(y);
	}

	ui->plot1->addGraph();
	ui->plot1->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot1->graph(0)->setData(xAxis, yAxis);
	ui->plot1->xAxis->setRange(0, 0.1);
	ui->plot1->yAxis->setRange(-1, 1);
	ui->plot1->replot();

	int FFTSize = 64;
	double FrequencyRate = 44100.0 / FFTSize;
	auto fft = Aquila::FftFactory::getFft(FFTSize);
	auto spectrum = fft->fft(yAxis.toStdVector().data());

	QVector<double> amplitude, frequency;
	int index = 0;
	for(auto item : spectrum)
	{
		amplitude.push_back(pow(item.real(), 2) + pow(item.imag(), 2));
		frequency.push_back((index++)*FrequencyRate);
	}

	ui->plot2->addGraph();
	ui->plot2->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
	ui->plot2->graph(0)->setData(frequency, amplitude);
	ui->plot2->xAxis->setRange(0, 50000);
	ui->plot2->yAxis->setRange(0, *std::max_element(amplitude.begin(), amplitude.end()));
	ui->plot2->replot();
}
